package model;

public class Chitanta 
{
	
    private String nume_pro;
    private String nume_cli;
    private int cantitate;
    private int cost;
    public Chitanta(String nume1,String nume2,int cant,int co)
    {
    	nume_pro=nume1;
    	nume_cli=nume2;
    	cantitate=cant;
    	cost=co;
    }
    public Chitanta()
    {
    	
    }
    public void set_nume_pro(String n)
    {
    	nume_pro=n;
    }
    public void set_nume_cli(String n)
    {
    	nume_cli=n;
    }
    public void set_cantitate(int n)
    {
    	cantitate=n;
    }
    public void set_cost(int n)
    {
    	cost=n;
    }
    public String get_nume_pro()
    {
    	return nume_pro;
    }
    public String get_nume_cli()
    {
    	return nume_cli;
    }
    public int get_cantitate()
    {
    	return cantitate;
    }
    public int get_cost()
    {
    	return cost;
    }
}
