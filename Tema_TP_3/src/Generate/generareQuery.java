package Generate;

import java.lang.reflect.Field;
import java.util.ArrayList;

import javax.swing.table.DefaultTableModel;

import com.mysql.fabric.xmlrpc.Client;

import model.Cli;

public class generareQuery 
{
 public static String create(Object tip,String[] cautari)
 {
	 String rezultat="select ",temp;
	 int v=0;
	 Class folosit=tip.getClass();
	 for (Field field : folosit.getDeclaredFields()) {
			if (v>=1) 
			{
				rezultat = rezultat + ",";
				v++;
			} 
			else 
			{
				v++;
			}
			rezultat = rezultat + field.getName();
		}
	    rezultat = rezultat + " "+ "from "+ folosit.getSimpleName();
		if (cautari != null) 
		{
			v = 0;
			rezultat = rezultat + " where ";
			for (String str : cautari) 
			{
				if (v>=1)
				{
					rezultat = rezultat + "&&";
					v++;
				}
				else
				{
					v++;
				}
				rezultat = rezultat + str;
			}
		}
		return rezultat;
 }
 public static<T> DefaultTableModel generate(ArrayList<T> data,T ajutor) throws IllegalArgumentException, IllegalAccessException
 {
	 DefaultTableModel rezultat=new DefaultTableModel();
	 int i=0;
	 for(Field field:ajutor.getClass().getDeclaredFields())
	 {
		 rezultat.addColumn(field);
		 i++;
	 }
	 Object ob[]=new Object[i];
	 i=0;
	 for(Field field:ajutor.getClass().getDeclaredFields())
	 {
		 ob[i]=field.getName();
		 i++;
	 }
	 rezultat.addRow(ob);
	 for(T obiect:data)
	 {
		 i=0;
		 for(Field field:obiect.getClass().getDeclaredFields())
		 {
			 field.setAccessible(true);
			 ob[i]=field.get(obiect).toString();
			 i++;
		 }
		 rezultat.addRow(ob);
	 }
	 return rezultat;
 }

 /*public static void main(String[] args) 
 {
	 Cli client=new Cli();
	 System.out.println(generareQuery.create(client,null));
 }*/
}
