package Connection;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.swing.JOptionPane;

import com.mysql.jdbc.Statement;

public class ConnectionFactory 
{
	 private static String Driver="com.mysql.jdbc.Driver";
	 private static String DB="jdbc:mysql://localhost:3306/tema";
	 private static String user ="root";
	 private static String pass="pandora";
	 private static java.sql.Connection conectiune;
	 private static  ConnectionFactory singleInstance=new ConnectionFactory();
	 private java.sql.Connection createConnection()
	 {
	    	try {
				conectiune=DriverManager.getConnection(DB,user,pass);
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				JOptionPane.showMessageDialog(null, "Eroare Creare Resultset", "Eroare", JOptionPane.ERROR_MESSAGE);
			} 
	    	return conectiune;
	 }
	 private ConnectionFactory()
	 {
		 try {
			Class.forName(Driver);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
		 this.createConnection();
	 }
	 public static java.sql.Connection getconnection()
	 {
		 return conectiune;
	 }
	 public static void close(java.sql.Connection con)
	 {
		 try {
			con.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	 }
	 public static void close(Statement statement)
	 {
		 try {
			 statement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	 }
	 public static void close(ResultSet result)
	 {
		 try {
			result.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
		}
	 }
}
