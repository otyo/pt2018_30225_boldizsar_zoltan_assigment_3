package Interface;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.lang.reflect.Field;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableModel;

import com.mysql.jdbc.Statement;

import Connection.ConnectionFactory;
import Generate.generareQuery;
import Validate.ValidateChitanta;
import Validate.ValidateClient;
import Validate.ValidateProduct;
import model.Chitanta;
import model.Cli;
import model.Pro;

public class Controller 
{
  private int temp;
  private ConnectionFactory conectiune;
  private java.sql.Statement st1;
  private java.sql.CallableStatement st2;
  private ResultSet rs;
  public String[] getClients()
  {
	  String[] rezultat=null;
	  try 
	{
		  int o=0;
		st1= ConnectionFactory.getconnection().createStatement();
		Cli client=new Cli();
		rs=st1.executeQuery(generareQuery.create(client,null));
		while(rs.next()) o++;
		rs.beforeFirst();
		rezultat=new String[o];
		o=0;
		while(rs.next())
		{
			rezultat[o]=rs.getString(1);
			o++;
		}
	} 
	  catch (Exception e) 
	{
		// TODO Auto-generated catch block
		System.out.println("Fail getClients");
	}
	  return rezultat;
  }
  public String[] getProducts()
  {
	  String[] rezultat=null;
	  try 
	{
		  int o=0;
		st1= ConnectionFactory.getconnection().createStatement();
		Pro produs=new Pro();
		rs=st1.executeQuery(generareQuery.create(produs,null));
		while(rs.next()) o++;
		rs.beforeFirst();
		rezultat=new String[o];
		o=0;
		while(rs.next())
		{
			rezultat[o]=rs.getString(1);
			o++;
		}
	} 
	  catch (Exception e) 
	{
		// TODO Auto-generated catch block
		System.out.println("Fail getProducts");
	}
	  return rezultat;
  }
  public String[] getChitante()
  {
	  String[] rezultat=null;
	  try 
	{
		  int o=0,u=0;
		st1= ConnectionFactory.getconnection().createStatement();
		Chitanta chitanta=new Chitanta();
		rs=st1.executeQuery(generareQuery.create(chitanta,null));
		while(rs.next()) o++;
		rs.beforeFirst();
		rezultat=new String[o+1];
		o=0;
		while(rs.next())
		{
			rezultat[o]=rs.getString(1)+","+rs.getString(2)+","+rs.getInt(3)+","+rs.getInt(4);
			o++;
		}
	} 
	  catch (Exception e) 
	{
		// TODO Auto-generated catch block
		System.out.println("Fail getProducts");
	}
	  return rezultat;
  }
  public void controlStergereChitanta(int id)
  {
	  try 
		{
		    st2=ConnectionFactory.getconnection().prepareCall("{call Chitanta_torol(?)}");
			st2.setInt(1,id);
			st2.execute();
			st2.close();
			 System.out.println("Succes StergereChitanta");
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println("Fail StergereChitanta");
		}
	 
  }
  public void controlStergereClient(String s1)
  {
	  
	  try 
		{
		    st2=ConnectionFactory.getconnection().prepareCall("{call Cli_torol(?)}");
			st2.setString(1,s1);
			st2.execute();
			st2.close();
			 System.out.println("Succes StergereClient");
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println("Fail StergereClient");
		}
  }
  public void controlStergereProdus(String s1)
  {
	  try 
		{
		    st2=ConnectionFactory.getconnection().prepareCall("{call Pro_torol(?)}");
			st2.setString(1,s1);
			st2.execute();
			st2.close();
			 System.out.println("Succes StergereProdus");
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println(e.getMessage());
			System.out.println("Fail StergereProdus");
		}
  }public void controlCautareChitanta(String s1,String s2,String s3,String s4,String s5,String s6,JPanel wrapper,JFrame frame)
  {
	  if(ValidateChitanta.validareCautare(s3,s4,s5,s6))
	  {
		  int temp2,temp3;
	  String[] restrictii=null;
	  wrapper.removeAll();
	  Chitanta mod=new Chitanta();
	  Class temp1=mod.getClass();
	  temp=0;
	  if(!s1.equals("")) temp++;
	  if(!s2.equals("")) temp++;
	  if(!s3.equals("")) temp++;
	  if(!s4.equals("")) temp++;
	  if(!s5.equals("")) temp++;
	  if(!s6.equals("")) temp++;

	  if(temp!=0)  restrictii=new String[temp];
	  temp=0;
	  if(!s1.equals("")) 
	  {
		  restrictii[temp]="nume_cli='"+s1+"'";
		  temp++;
	  }
	  if(!s2.equals(""))
	  {
		  restrictii[temp]="nume_pro='"+s3+"'";
		  temp++;
	  }
	  if(!s3.equals("")) 
	  {
		  restrictii[temp]="cantitate>"+s3;
		  temp++;
	  }
	  if(!s4.equals("")) 
	  {
		  restrictii[temp]="cantitate<"+s4;
		  temp++;
	  }
	  if(!s5.equals(""))
		  {
			  restrictii[temp]="cost>"+s5;
			  temp++;
		  }
	  if(!s6.equals(""))
		  {
			  restrictii[temp]="cost<"+s6;
			  temp++;
		  }
	  try 
		{
		
		  temp=0;
			st1=ConnectionFactory.getconnection().createStatement();
			rs=st1.executeQuery(generareQuery.create(mod,restrictii));
			ArrayList<Chitanta> date=new ArrayList<Chitanta>();
			while(rs.next())
			{
				date.add(new Chitanta(rs.getString(1),rs.getString(2),rs.getInt(3),rs.getInt(4)));
			}
			JTable table=null;
			try {
				table = new JTable(generareQuery.generate(date,mod));
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			wrapper.add(table);
			
		} 
	     catch (SQLException e) 
		{
			System.out.println(e.getMessage());
			// TODO Auto-generated catch block
			System.out.println("Fail Cautare");
		}
	  frame.revalidate();
	  frame.repaint();
	  }
	  else
	  {
		  System.out.println("Fail 1 CautareProdus");
	  }
  }
  public void controlCautareProdus(String s1,String s2,String s3,String s4,String s5,String s6,String s7,String s8,String s9,String s10,JPanel wrapper,JFrame frame)
  {
	  if(ValidateProduct.validareCautare(s7,s8,s9,s10))
	  {
		  int temp2,temp3;
	  String[] restrictii=null;
	  wrapper.removeAll();
	  Pro mod=new Pro();
	  Class temp1=mod.getClass();
	  temp=0;
	  DefaultTableModel Model = new DefaultTableModel();
	  for (Field field : temp1.getDeclaredFields())
		  {
		  Model.addColumn(field.getName());
		  temp++;
		  }
	  temp2=temp;
	  Object[] tim=new Object[temp2];
	  temp=0;
	  for (Field field : temp1.getDeclaredFields()) 
		  {
		  tim[temp]=field.getName();
		  temp++;
		  }
	  Model.addRow(tim);
	  temp=0;
	  if(!s1.equals("")) temp++;
	  if(!s2.equals("")) temp++;
	  if(!s3.equals("")) temp++;
	  if(!s4.equals("")) temp++;
	  if(!s5.equals("")) temp++;
	  if(!s6.equals("")) temp++;
	  if(!s7.equals("")) temp++;
	  if(!s8.equals("")) temp++;
	  if(!s9.equals("")) temp++;
	  if(!s10.equals("")) temp++;
	  if(temp!=0)  restrictii=new String[temp];
	  temp=0;
	  if(!s1.equals("")) 
	  {
		  restrictii[temp]="nume like '"+s1+"%'";
		  temp++;
	  }
	  if(!s2.equals(""))
	  {
		  restrictii[temp]="nume like '%"+s2+"%'";
		  temp++;
	  }
	  if(!s3.equals("")) 
	  {
		  restrictii[temp]="nume like '%"+s3+"'";
		  temp++;
	  }
	  if(!s4.equals("")) 
	  {
		  restrictii[temp]="tip like '"+s4+"%'";
		  temp++;
	  }
	  if(!s5.equals(""))
		  {
			  restrictii[temp]="tip like '%"+s5+"%'";
			  temp++;
		  }
	  if(!s6.equals(""))
		  {
			  restrictii[temp]="tip like '%"+s6+"'";
			  temp++;
		  }
	  if(!s7.equals(""))
	  {
		  restrictii[temp]="pret>"+s7;
		  temp++;
	  }
	  if(!s8.equals("")) 
	  {
		  restrictii[temp]="pret<"+s8;
		  temp++;
	  }
	  if(!s9.equals("")) 
	  {
		  restrictii[temp]="stoc>"+s9;
		  temp++;
	  }
	  if(!s10.equals(""))
	  {
		  restrictii[temp]="stoc<"+s10;
		  temp++;
	  }
	  try 
		{
		  temp=0;
		  st1=ConnectionFactory.getconnection().createStatement();
		  rs=st1.executeQuery(generareQuery.create(mod,restrictii));
		  ArrayList<Pro> date=new ArrayList<Pro>();
			while(rs.next())
			{
				date.add(new Pro(rs.getString(1),rs.getString(2),rs.getInt(3),rs.getInt(4)));
			}
			JTable table=null;
			try {
				table = new JTable(generareQuery.generate(date,mod));
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			wrapper.add(table);
			
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("Fail Cautare");
		}
	  frame.revalidate();
	  frame.repaint();
	  }
	  else
	  {
		  System.out.println("Fail 1 CautareProdus");
	  }
  }
  public void controlCautareClient(String s1,String s2,String s3,String s4,String s5,String s6,String s7,String s8,String s9,String s10,JPanel wrapper,JFrame frame)
  {
	  if(ValidateProduct.validareCautare(s7,s8,s9,s10))
	  {
		  int temp2,temp3;
	  String[] restrictii=null;
	  wrapper.removeAll();
	  Cli mod=new Cli();
	  Class temp1=mod.getClass();
	  temp=0;
	  DefaultTableModel Model = new DefaultTableModel();
	  for (Field field : temp1.getDeclaredFields()) 
		  {
		  temp++;
		  Model.addColumn(field.getName());
		  }
	  temp2=temp;
	  Object[] tim=new Object[temp2];
	  temp=0;
	  for (Field field : temp1.getDeclaredFields()) 
		  {
		  tim[temp]=field.getName();
		  temp++;
		  }
	  Model.addRow(tim);
	  temp=0;
	  if(!s1.equals("")) temp++;
	  if(!s2.equals("")) temp++;
	  if(!s3.equals("")) temp++;
	  if(!s4.equals("")) temp++;
	  if(!s5.equals("")) temp++;
	  if(!s6.equals("")) temp++;
	  if(!s7.equals("")) temp++;
	  if(!s8.equals("")) temp++;
	  if(!s9.equals("")) temp++;
	  if(!s10.equals("")) temp++;
	  if(temp!=0)  restrictii=new String[temp];
	  temp=0;
	  if(!s1.equals("")) 
	  {
		  restrictii[temp]="nume like '"+s1+"%'";
		  temp++;
	  }
	  if(!s2.equals(""))
	  {
		  restrictii[temp]="nume like '%"+s2+"%'";
		  temp++;
	  }
	  if(!s3.equals("")) 
	  {
		  restrictii[temp]="nume like '%"+s3+"'";
		  temp++;
	  }
	  if(!s4.equals("")) 
	  {
		  restrictii[temp]="prenume like '"+s4+"%'";
		  temp++;
	  }
	  if(!s5.equals(""))
		  {
			  restrictii[temp]="prenume like '%"+s5+"%'";
			  temp++;
		  }
	  if(!s6.equals(""))
		  {
			  restrictii[temp]="prenume like '%"+s6+"'";
			  temp++;
		  }
	  if(!s7.equals(""))
	  {
		  restrictii[temp]="serial_number>"+s7;
		  temp++;
	  }
	  if(!s8.equals("")) 
	  {
		  restrictii[temp]="serial_number<"+s8;
		  temp++;
	  }
	  if(!s9.equals("")) 
	  {
		  restrictii[temp]="buget>"+s9;
		  temp++;
	  }
	  if(!s10.equals(""))
	  {
		  restrictii[temp]="buget<"+s10;
		  temp++;
	  }
	  try 
		{
		  temp=0;
		  st1=ConnectionFactory.getconnection().createStatement();
		  rs=st1.executeQuery(generareQuery.create(mod,restrictii));
		  ArrayList<Cli> date=new ArrayList<Cli>();
			while(rs.next())
			{
				date.add(new Cli(rs.getString(1),rs.getString(2),rs.getInt(3),rs.getInt(4)));
			}
			JTable table=null;
			try {
				table = new JTable(generareQuery.generate(date,mod));
			}  catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			wrapper.add(table);
			
		} catch (SQLException e) 
		{
			// TODO Auto-generated catch block
			System.out.println("Fail Cautare");
		}
	  frame.revalidate();
	  frame.repaint();
	  }
	  else
	  {
		  System.out.println("Fail 1 CautareProdus");
	  }
  }

  
  public void controlCreareChitanta(String s1,String s2,String s3) throws NumberFormatException, SQLException,Exception
  {
	  if(ValidateChitanta.validareCreare(s1,s2,s3))
	  {
		  try 
		{
			  String[] t={"nume='"+s2+"'"};
			  Pro produs=new Pro();
			rs=st1.executeQuery(generareQuery.create(produs,t));
			st2=ConnectionFactory.getconnection().prepareCall("{call Chitanta_letrehoz(?,?,?)}");
		} catch (SQLException e) 
		  {
			// TODO Auto-generated catch block
			System.out.println("Fail 2 CreareChitanta");
		}
		  int t=0,t1=0;;
		  if(rs.next()) 
			  {
			   t=rs.getInt(4);
			   t1=rs.getInt(3);
			  }
		  if(t>=Integer.parseInt(s3))
		  {
		  try 
			{
			  BufferedWriter writer = new BufferedWriter(new FileWriter("achitanta.txt"));
			 writer.write("Clientul cu numele "+s1+" a cumparat "+Integer.parseInt(s3)+" produse cu numele "+s2+" in valoare de "+Integer.parseInt(s3)*t1+System.lineSeparator());
			 writer.close();
				st2.setString(1, s1);
				st2.setString(2, s2);
				st2.setInt(3,Integer.parseInt(s3));
				st2.execute();
				st2.close();
				st2=ConnectionFactory.getconnection().prepareCall("{call Pro_levon(?,?)}");
				st2.setString(1, s2);
				st2.setInt(2,Integer.parseInt(s3));
				st2.execute();
				st2.close();
				 System.out.println("Succes CreareChitanta");
			} catch (Exception e) 
			{
				// TODO Auto-generated catch block
				System.out.println("Fail 3 CreareChitanta");
			}
		  }
		  else
		  {
			  System.out.println("Eroare cantiatate");
		  }
		 
	  }
	  else
	  {
		  System.out.println("Fail 1 CreareChitanta");
	  }
  }
  public void controlCreareClient(String s1,String s2,String s3,String s4)
  {
	  if(ValidateClient.validareCreare(s1,s2,s3,s4))
	  {
		  try 
			{
				st2=ConnectionFactory.getconnection().prepareCall("{call Cli_letrehoz(?,?,?,?)}");
				st2.setString(1, s1);
				st2.setString(2, s2);
				st2.setInt(3, Integer.parseInt(s3));
				st2.setInt(4, Integer.parseInt(s4));
				st2.execute();
				st2.close();
				System.out.println("Succes CreareClient");
			} 
		    catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("Fail 1 CreareClient");
			}
	  }
  }
  public void controlCreareProdus(String s1,String s2,String s3,String s4)
  {
	  if(ValidateProduct.validareCreare(s1,s2,s3,s4))
	  {
		  try 
			{
				st2=ConnectionFactory.getconnection().prepareCall("{call Pro_letrehoz(?,?,?,?)}");
				st2.setString(1, s1);
				st2.setString(2, s2);
				st2.setInt(3, Integer.parseInt(s3));
				st2.setInt(4, Integer.parseInt(s4));
				st2.execute();
				st2.close();
				System.out.println("Succes CreareProdus");
			} 
		    catch (SQLException e) 
			{
				// TODO Auto-generated catch block
				System.out.println("Fail 1 CreareProdus");
			}
	  }
  }
  public<T> String[] getFields(T obiect)
  {
	  String[] rezultat;
	  int i=0;
	  for(Field field:obiect.getClass().getDeclaredFields())
	  {
		  if(!field.getName().equals("nume")) i++;
	  }
	  rezultat=new String[i];
	  i=0;
	  for(Field field:obiect.getClass().getDeclaredFields())
	  {
		  if(!field.getName().equals("nume"))
	   {
		  rezultat[i]=field.getName();
		  i++;
	   }
		  
	  }
	  return rezultat;
  }
  public<T> String getdemodificat(T obiect,String s1,String s2,int op)
  {
	  String rezultat=null;
	  try 
	  {
		st1= ConnectionFactory.getconnection().createStatement();
		rs=st1.executeQuery("select "+s2+ " from "+obiect.getClass().getSimpleName()+" where nume='"+s1+"'");
		if(op==0)
		{
		if(rs.next()) rezultat=(String) rs.getObject(1);
		}
		else
		{
			if(rs.next()) rezultat=rs.getObject(1).toString();
		}
	  } 
	  catch (Exception e) 
	  {
		  System.out.println("Fail Get");
		e.printStackTrace();
	   }
	 return rezultat;
  }
  public<T> boolean setdemodificat(T obiect,String s1,String s2,int op,String s3)
  {
	  boolean rezultat=false;
	  try 
	  {
		st1= ConnectionFactory.getconnection().createStatement();
		if(op==0)
		{
			rezultat=st1.execute("update "+obiect.getClass().getSimpleName()+ " set "+obiect.getClass().getSimpleName()+"."+s2+"='"+s3+"' where nume='"+s1+"'");
		}
		else
		{
			rezultat=st1.execute("update "+obiect.getClass().getSimpleName()+ " set "+obiect.getClass().getSimpleName()+"."+s2+"="+s3+" where nume='"+s1+"'");
		}
		
		
	  } 
	  catch (Exception e) 
	  {
		  System.out.println("Fail Set");
		e.printStackTrace();
	   }
	 return rezultat;
  }
}
