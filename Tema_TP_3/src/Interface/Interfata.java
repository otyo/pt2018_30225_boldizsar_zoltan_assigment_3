package Interface;

import java.awt.FlowLayout;
import java.awt.Menu;
import java.awt.MenuBar;
import java.awt.MenuItem;
import java.awt.MenuShortcut;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.sql.SQLException;

import javax.swing.BoxLayout;
import javax.swing.JButton;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JMenu;
import javax.swing.JMenuBar;
import javax.swing.JMenuItem;
import javax.swing.JPanel;
import javax.swing.JTextField;

import model.Cli;
import model.Pro;

public class Interfata extends JFrame implements ActionListener 
{
	private Controller control=new Controller();
	private JComboBox box1,box2;
	private JMenuBar menu;
	private JMenu clienti,produse,chitanta;
	private JMenuItem cclient,cprouse,cchitanta,sclient,sprouse,schitanta,dclient,dprouse,dchitanta,mclient,mprouse;
	private JPanel pan1,pan2,pan3,pan4,pan5,wrapper;
	private JTextField text1,text2,text3,text4,text5,text6,text7,text8,text9,text10;
	private JLabel l1,l2,l3,l4,l5,l6,l7,l8,l9,l10,l11,l12;
	private JButton start;
	public Interfata()
	{
		text1=new JTextField(20);
		text2=new JTextField(20);
		text3=new JTextField(20);
		text4=new JTextField(20);
		text5=new JTextField(20);
		text6=new JTextField(20);
		text7=new JTextField(20);
		text8=new JTextField(20);
		text9=new JTextField(20);
		text10=new JTextField(20);
		this.setTitle("Interfata");
	  	this.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	  	this.setLocationRelativeTo(null);
	  	this.setSize(400,400);
	  	wrapper=new JPanel();
	  	wrapper.setLayout(new BoxLayout(wrapper,BoxLayout.Y_AXIS));
	  	this.add(wrapper);
	  	menu=new JMenuBar();
	  	clienti=new JMenu("Clienti");
	  	cclient=new JMenuItem("Creare Clienti");
	  	cclient.addActionListener(this);
	  	sclient=new JMenuItem("Cautare Clienti");
	  	sclient.addActionListener(this);
	  	mclient=new JMenuItem("Modificare Clienti");
	  	mclient.addActionListener(this);
	  	dclient=new JMenuItem("Stergere Clienti");
	  	dclient.addActionListener(this);
	  	clienti.add(cclient);
	  	clienti.add(sclient);
	  	clienti.add(mclient);
	  	clienti.add(dclient);
	  	menu.add(clienti);
	  	produse=new JMenu("Produse");
	  	cprouse=new JMenuItem("Creare Produse");
	  	cprouse.addActionListener(this);
	  	sprouse=new JMenuItem("Cautare Produse");
	  	sprouse.addActionListener(this);
	  	mprouse=new JMenuItem("Modificare Produse");
	  	mprouse.addActionListener(this);
	  	dprouse=new JMenuItem("Stergere Produse");
	  	dprouse.addActionListener(this);
	  	produse.add(cprouse);
	  	produse.add(sprouse);
	  	produse.add(mprouse);
	  	produse.add(dprouse);
	  	menu.add(produse);
	  	chitanta=new JMenu("Chitante");
	  	cchitanta=new JMenuItem("Creare Chitanta");
	  	cchitanta.addActionListener(this);
	  	schitanta=new JMenuItem("Cautare Chitanta");
	  	schitanta.addActionListener(this);
	  	dchitanta=new JMenuItem("Stergere Chitanta");
	  	dchitanta.addActionListener(this);
	  	chitanta.add(cchitanta);
	  	chitanta.add(schitanta);
	  	chitanta.add(dchitanta);
	  	menu.add(chitanta);
	  	this.setJMenuBar(menu);
	  	this.setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent eve) 
	{
		String com=eve.getActionCommand();
		if(com=="Creare Clienti")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			pan5=new JPanel();
			pan5.setLayout(new FlowLayout());
			l1=new JLabel("Nume:");
			l2=new JLabel("Prenume:");
			l3=new JLabel("Serial-number:");
			l4=new JLabel("Buget:");
			start=new JButton("Creare Client Nou");
			start.addActionListener(this);
			pan1.add(l1);
			pan1.add(text1);
			pan2.add(l2);
			pan2.add(text2);
			pan3.add(l3);
			pan3.add(text3);
			pan4.add(l4);
			pan4.add(text4);
			pan5.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			wrapper.add(pan5);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Creare Produse")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			pan5=new JPanel();
			pan5.setLayout(new FlowLayout());
			l1=new JLabel("Nume:");
			l2=new JLabel("Tip:");
			l3=new JLabel("Pret:");
			l4=new JLabel("Stoc:");
			start=new JButton("Creare Produs Nou");
			start.addActionListener(this);
			pan1.add(l1);
			pan1.add(text1);
			pan2.add(l2);
			pan2.add(text2);
			pan3.add(l3);
			pan3.add(text3);
			pan4.add(l4);
			pan4.add(text4);
			pan5.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			wrapper.add(pan5);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Creare Chitanta")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			box1=new JComboBox(control.getClients());
			box2=new JComboBox(control.getProducts());
			start=new JButton("Creare Chitanta Nou");
			start.addActionListener(this);
			l1=new JLabel("Nume Client:");
			l2=new JLabel("Nume Produs:");
			l3=new JLabel("Cantitate:");
			pan1.add(l1);
			pan1.add(box1);
			pan2.add(l2);
			pan2.add(box2);
			pan3.add(l3);
			pan3.add(text1);
			pan4.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Cautare Clienti")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			pan5=new JPanel();
			pan5.setLayout(new FlowLayout());
			start=new JButton("Cautare Client acum");
			start.addActionListener(this);
			l1=new JLabel("Nume client inceput:");
			l2=new JLabel("Nume client contine:");
			l3=new JLabel("Nume client sfarsit:");
			l4=new JLabel("Preume client inceput:");
			l5=new JLabel("Preume client contine:");
			l6=new JLabel("Preume client sfarsit:");
			l7=new JLabel("Serial number min:");
			l8=new JLabel("Serial number max:");
			l9=new JLabel("Buget min:");
			l10=new JLabel("Buget max:");
			pan1.add(l1);
			pan1.add(text1);
			pan1.add(l2);
			pan1.add(text2);
			pan1.add(l3);
			pan1.add(text3);
			pan2.add(l4);
			pan2.add(text4);
			pan2.add(l5);
			pan2.add(text5);
			pan2.add(l6);
			pan2.add(text6);
			pan3.add(l7);
			pan3.add(text7);
			pan3.add(l8);
			pan3.add(text8);
			pan4.add(l9);
			pan4.add(text9);
			pan4.add(l10);
			pan4.add(text10);
			pan5.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			wrapper.add(pan5);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Cautare Produse")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			pan5=new JPanel();
			pan5.setLayout(new FlowLayout());
			start=new JButton("Cautare Produs acum");
			start.addActionListener(this);
			l1=new JLabel("Nume produs inceput:");
			l2=new JLabel("Nume produs contine:");
			l3=new JLabel("Nume produs sfarsit:");
			l4=new JLabel("Nume tip inceput:");
			l5=new JLabel("Nume tip contine:");
			l6=new JLabel("Nume tip sfarsit:");
			l7=new JLabel("Pret min:");
			l8=new JLabel("Pret max:");
			l9=new JLabel("Stoc min:");
			l10=new JLabel("Stoc max:");
			pan1.add(l1);
			pan1.add(text1);
			pan1.add(l2);
			pan1.add(text2);
			pan1.add(l3);
			pan1.add(text3);
			pan2.add(l4);
			pan2.add(text4);
			pan2.add(l5);
			pan2.add(text5);
			pan2.add(l6);
			pan2.add(text6);
			pan3.add(l7);
			pan3.add(text7);
			pan3.add(l8);
			pan3.add(text8);
			pan4.add(l9);
			pan4.add(text9);
			pan4.add(l10);
			pan4.add(text10);
			pan5.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			wrapper.add(pan5);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Modificare Clienti")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			box1=new JComboBox(control.getClients());
			box2=new JComboBox(control.getFields(new Cli()));
			start=new JButton("Alege Client");
			start.addActionListener(this);
			l1=new JLabel("Client de modificat:");
			l2=new JLabel("Data de modificat:");
			pan1.add(l1);
			pan1.add(box1);
			pan1.add(l2);
			pan1.add(box2);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Alege Client")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			text1=new JTextField(20);
			text1.setText(control.getdemodificat(new Cli(),(String) box1.getSelectedItem(),(String) box2.getSelectedItem(),box2.getSelectedIndex()));
			start=new JButton("Modifica Client");
			start.addActionListener(this);
			l1=new JLabel("Data:");
			pan1.add(l1);
			pan1.add(text1);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Modifica Client")
		{
			control.setdemodificat(new Cli(),(String) box1.getSelectedItem(),(String) box2.getSelectedItem(),box2.getSelectedIndex(),text1.getText());
		}
		else if(com=="Modificare Produse")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			box1=new JComboBox(control.getProducts());
			box2=new JComboBox(control.getFields(new Pro()));
			start=new JButton("Alege Produs");
			start.addActionListener(this);
			l1=new JLabel("Produs de modificat:");
			l2=new JLabel("Data de modificat:");
			pan1.add(l1);
			pan1.add(box1);
			pan1.add(l2);
			pan1.add(box2);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Alege Produs")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			text1=new JTextField(20);
			text1.setText(control.getdemodificat(new Pro(),(String) box1.getSelectedItem(),(String) box2.getSelectedItem(),box2.getSelectedIndex()));
			start=new JButton("Modifica Produs");
			start.addActionListener(this);
			l1=new JLabel("Data:");
			pan1.add(l1);
			pan1.add(text1);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Modifica Produs")
		{
			
		}
		else if(com=="Cautare Chitanta")
		{
			wrapper.removeAll();
			text1=new JTextField(20);
			text2=new JTextField(20);
			text3=new JTextField(20);
			text4=new JTextField(20);
			text5=new JTextField(20);
			text6=new JTextField(20);
			text7=new JTextField(20);
			text8=new JTextField(20);
			text9=new JTextField(20);
			text10=new JTextField(20);
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan3=new JPanel();
			pan3.setLayout(new FlowLayout());
			pan4=new JPanel();
			pan4.setLayout(new FlowLayout());
			pan5=new JPanel();
			pan5.setLayout(new FlowLayout());
			box1=new JComboBox(control.getClients());
			box2=new JComboBox(control.getProducts());
			start=new JButton("Cautare Chitanta acum");
			start.addActionListener(this);
			l1=new JLabel("Client:");
			l2=new JLabel("Produs:");
			l3=new JLabel("Cantiate min:");
			l4=new JLabel("Cantiate max:");
			l5=new JLabel("Cost min:");
			l6=new JLabel("Cost max:");
			pan1.add(l1);
			pan1.add(box1);
			pan2.add(l2);
			pan2.add(box2);
			pan3.add(l3);
			pan3.add(text3);
			pan3.add(l4);
			pan3.add(text4);
			pan4.add(l5);
			pan4.add(text5);
			pan4.add(l6);
			pan4.add(text6);
			pan5.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			wrapper.add(pan3);
			wrapper.add(pan4);
			wrapper.add(pan5);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Stergere Chitanta")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			box1=new JComboBox(control.getChitante());
			start=new JButton("Sterge Chitanta acum");
			start.addActionListener(this);
			l1=new JLabel("Chitanta de sters:");
			pan1.add(l1);
			pan1.add(box1);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Stergere Produse")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			box1=new JComboBox(control.getProducts());
			start=new JButton("Sterge Produs acum");
			start.addActionListener(this);
			l1=new JLabel("Produs de sters:");
			pan1.add(l1);
			pan1.add(box1);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Stergere Clienti")
		{
			wrapper.removeAll();
			pan1=new JPanel();
			pan1.setLayout(new FlowLayout());
			pan2=new JPanel();
			pan2.setLayout(new FlowLayout());
			box1=new JComboBox(control.getClients());
			start=new JButton("Sterge Client acum");
			start.addActionListener(this);
			l1=new JLabel("Client de sters:");
			pan1.add(l1);
			pan1.add(box1);
			pan2.add(start);
			wrapper.add(pan1);
			wrapper.add(pan2);
			this.pack();
			this.revalidate();
			this.repaint();
		}
		else if(com=="Creare Chitanta Nou")
		{
			try {
				control.controlCreareChitanta((String) box1.getSelectedItem(),(String) box2.getSelectedItem(),text1.getText());
			} catch (Exception e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		else if(com=="Creare Client Nou")
		{
			control.controlCreareClient(text1.getText(),text2.getText(),text3.getText(),text4.getText());
		}
		else if(com=="Creare Produs Nou")
		{
			control.controlCreareProdus(text1.getText(),text2.getText(),text3.getText(),text4.getText());
		}
		else if(com=="Sterge Produs acum")
		{
			control.controlStergereProdus((String) box1.getSelectedItem());
		}
		else if(com=="Sterge Client acum")
		{
			control.controlStergereClient((String) box1.getSelectedItem());
		}
		else if(com=="Sterge Chitanta acum")
		{
			control.controlStergereChitanta(box1.getSelectedIndex());
		}
		else if(com=="Cautare Chitanta acum")
		{
			control.controlCautareChitanta(text1.getText(),text2.getText(),text3.getText(),text4.getText(),text5.getText(),text6.getText(), wrapper, this);
		}
		else if(com=="Cautare Client acum")
		{
			control.controlCautareClient(text1.getText(),text2.getText(),text3.getText(),text4.getText(),text5.getText(),text6.getText(),text7.getText(),text8.getText(),text9.getText(),text10.getText(),wrapper,this);
		}
		else if(com=="Cautare Produs acum")
		{
			control.controlCautareProdus(text1.getText(),text2.getText(),text3.getText(),text4.getText(),text5.getText(),text6.getText(),text7.getText(),text8.getText(),text9.getText(),text10.getText(),wrapper,this);
		}
	}
	public static void main(String[] args) 
    {
		Interfata Interfata=new Interfata();
    }
}
