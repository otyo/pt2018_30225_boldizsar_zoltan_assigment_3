drop database if exists tema;
create database tema;
use tema;

drop table if exists Pro;
create table if not exists Pro
(
nume varchar(20)unique primary key,
tip varchar(20),
pret int,
stoc int
);

drop table if exists Cli;
create table if not exists Cli
(
nume varchar(20)unique primary key,
prenume varchar(20) ,
serial_number int,
buget int
);

drop table if exists Chitanta;
create table if not exists Chitanta
(
id_chitanta int auto_increment unique primary key,
nume_pro varchar(20),
nume_cli varchar(20),
cantitate int,
cost int
);

insert into  Pro(nume,tip,pret,stoc) values
('Looney Toons','Papusa',10,100),
('Duckey Duck','Papusa',8,100),
('Ferrari','Masina',15,100),
('Camion','Masina',9,100);

insert into  Cli(nume,prenume,serial_number,buget) values
('Alpar','Ravasz',1,100),
('Csiszer','Ors',2,3),
('Boldizsar','Noemi',3,10),
('Tintar','Ana',4,50);

insert into  Chitanta(nume_pro,nume_cli,cantitate,cost) values
('Duckey Duck','Tintar',2,16),
('Ferrari','Alpar',4,60);

select * from Cli;
select * from Pro;
select * from Chitanta;